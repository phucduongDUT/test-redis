import redis

host = '127.0.0.1'
port = '6379'
db = 0

r_0 = redis.Redis(host=host,
                port=port,
                db=0)

r_1 = redis.Redis(host=host,
                port=port,
                db=1)

r_test = redis.Redis(host=host,
                port=port,
                db=15)

# transfer list to hash
# hash_key = "s_11"
# for subkey in r_0.scan_iter("s_11*"):
#     # delete the key
#     print(subkey)
#     value = r_0.lrange(subkey, 0, -1)
#     print(value)
#     value = [str(v) for v in value]
#     r_test.hset(hash_key, subkey, ','.join(value))



# add value hash
# r_test.hset("NumberVsString", "1", "One")
# r_test.hset("NumberVsString", "2", "Two")
# r_test.hset("NumberVsString", "3", "Three")

# retrieve hash
# get_hash = r_test.hgetall('s_11')
# print('a')


# transfer old db(list) to new db (hash)
def transfer_db_to_hash(r_root, r_child, key_prefix):
    key_search = key_prefix + '*'
    for subkey in r_root.scan_iter(key_search):
        # delete the key
        #print(subkey)
        value = r_root.lrange(subkey, 0, -1)
        value = [str(v) for v in value]
        r_child.hset(key_prefix, subkey, ','.join(value))
        print('add key: ' + str(subkey) + ' value: ' + ','.join(value))


def retrieve_hash_key(key_hash):
    get_hash = r_test.hgetall(key_hash)
    print(get_hash)
    # return get_hash


key_hash = 's_11'
#transfer_db_to_hash(r_0, r_test, key_hash)
retrieve_hash_key(key_hash)

def new_line():
    return 0











